# Plantillas Flowblade

Estas plantillas son una adaptación para
[Flowblade](https://jliljebl.github.io/flowblade/) de las plantillas
creadas para el software propietario [Sony Vegas
Pro](https://www.vegascreativesoftware.com/us=/) por [Delim
Produciones](https://delimproductions.jimdo.com/templates-free/) y que
de forma gratuita puedes descargar desde su pagina web.
